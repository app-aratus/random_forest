import pandas
from sklearn import model_selection
from sklearn.ensemble import RandomForestClassifier

aline = "-------------------------------------------------------------------------------"

def describeDataFrame(dataframe):
    print( " " )
    print( aline )
    print( "dataframe.shape[1] col count")
    print(  dataframe.shape[1] )
    print( "dataframe.shape[0] row count")
    print(  dataframe.shape[0] )
    print( aline )
    print( "dataframe.head(5)")
    print(  dataframe.head(5))
    print( aline )
    print( "dataframe.describe()")
    print(  dataframe.describe())
    print( aline )

def main():

    # Input file
    csvFile      = 'pima-indians-diabetes-data.csv'

    # Pandas dataframe
    dataframe    = pandas.read_csv( csvFile, delimiter = ',') 

    # Describe the dataframe
    describeDataFrame(dataframe)

    # Dataframe values
    array        = dataframe.values

    # Features
    X            = array[:,0:8]

    # Response vector
    Y            = array[:,8]

    # Set a random state so we get the same accuracy every time we run.
    randomstate  = 7

    # Set 10 folds
    kfold = model_selection.KFold(n_splits=10, random_state=randomstate)

    # Define a classifier / model
    model = RandomForestClassifier(n_estimators=50, max_features=0.25, random_state=randomstate)

    # Get the cross validation score
    results = model_selection.cross_val_score(model, X, Y, cv=kfold)

    # Get the mean of the cross validation score
    print("The following is the mean of the cross validation score")
    print(results.mean())
    print(aline)

    # Returns
    # 0.7679596719070403


# This where the Python script starts
if __name__ == '__main__':
    main()
