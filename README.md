# random_forest


README


A Random Forest is an extremely powerful machine learning algorithm, also known as a type of bagged (bootstrap aggregation) decision tree.  

This code utilizes a Random Forest to classify sample data.

It utilizes a classic machine learning medical data set for the Pima Indians to predict whether or not they will have an onset of diabetes within 5 years.  This is a predictive modeling classification problem.  All the input columns are numerical.  The last column is the known result which is binary:  0 or 1.

This code utilizes K-fold cross validation.  In our concrete example through code we utilize K = 10 folds to score the accuracy of our trained model.  What that means is that our machine learning model trains on 9 folds of data and then tests on 1 fold.  Then we train on a different set of 9 folds and test on a different 1 fold.  This is repeated K times, then we take the mean average to derive the cross validation accuracy score of our model.  The concept is that we want to avoid overfitting (using the same data for training and testing the model).  We want the model to train on one set of data and test itself on another set of data which was held out of the training.

You'll need Python 3 and these third party libraries installed.

pip install -U scikit-learn

pip install pandas

This how to run it, and the output.

python random_forest.py
 
-------------------------------------------------------------------------------
dataframe.shape[1] col count
9
dataframe.shape[0] row count
767
-------------------------------------------------------------------------------
dataframe.head(5)
   6  148  72  35    0  33.6  0.627  50  1
0  1   85  66  29    0  26.6  0.351  31  0
1  8  183  64   0    0  23.3  0.672  32  1
2  1   89  66  23   94  28.1  0.167  21  0
3  0  137  40  35  168  43.1  2.288  33  1
4  5  116  74   0    0  25.6  0.201  30  0
-------------------------------------------------------------------------------
dataframe.describe()
                6         148          72          35           0        33.6       0.627          50           1
count  767.000000  767.000000  767.000000  767.000000  767.000000  767.000000  767.000000  767.000000  767.000000
mean     3.842243  120.859192   69.101695   20.517601   79.903520   31.990482    0.471674   33.219035    0.348110
std      3.370877   31.978468   19.368155   15.954059  115.283105    7.889091    0.331497   11.752296    0.476682
min      0.000000    0.000000    0.000000    0.000000    0.000000    0.000000    0.078000   21.000000    0.000000
25%      1.000000   99.000000   62.000000    0.000000    0.000000   27.300000    0.243500   24.000000    0.000000
50%      3.000000  117.000000   72.000000   23.000000   32.000000   32.000000    0.371000   29.000000    0.000000
75%      6.000000  140.000000   80.000000   32.000000  127.500000   36.600000    0.625000   41.000000    1.000000
max     17.000000  199.000000  122.000000   99.000000  846.000000   67.100000    2.420000   81.000000    1.000000
-------------------------------------------------------------------------------
The following is the mean of the cross validation score
0.761431989063568
-------------------------------------------------------------------------------




